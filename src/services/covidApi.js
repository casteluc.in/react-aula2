import axios from 'axios'

const api = axios.create({
    baseURL: "https://covid-19-coronavirus-statistics.p.rapidapi.com/"
})

let config = {
    headers: {
        "x-rapidapi-key": "69deb3f6ccmsh98af25966ce620ep147470jsn21ffbb1597ad",
        "x-rapidapi-host": "covid-19-coronavirus-statistics.p.rapidapi.com",
        "useQueryString": true
    }
}

async function pega_dados_covid(setEstado) {
    await api.get("/v1/total", config)
        .then( response => {
            setEstado(response.data)
        }).catch(error => {
            console.log(error)
        })
}

export {pega_dados_covid}