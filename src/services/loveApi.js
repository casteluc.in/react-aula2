import axios from 'axios'

const api = axios.create({
    baseURL: "https://love-calculator.p.rapidapi.com/"
})

let config = {
    headers: {
        'x-rapidapi-key': '69deb3f6ccmsh98af25966ce620ep147470jsn21ffbb1597ad',
        'x-rapidapi-host': 'love-calculator.p.rapidapi.com'
    }
}

async function calcula_porcentagem(fname, sname, setResposta) {
    config["params"] = {fname: fname, sname: sname}
    
    await api.get("/getPercentage", config)
        .then( response => {
            setResposta(response.data)
        }).catch( error => {
            console.log(error)
        })
}

export { calcula_porcentagem }