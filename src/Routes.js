import React from 'react'
import Covid from './pages/Covid/Covid'
import Lovometro from './pages/Lovometro/Lovometro'
import {BrowserRouter, Switch, Route, Redirect} from 'react-router-dom'
import NotFound from './pages/NotFound/NotFound'

function Routes() {
    return(
        <BrowserRouter>
            <Switch>
                <Route exact path="/">
                    <Redirect to="/lovometro"></Redirect>
                </Route>
                <Route exact path="/lovometro">
                    <Lovometro/>
                </Route>

                <Route exact path="/covid">
                    <Covid/>
                </Route>

                <Route path="/">
                    <NotFound />
                </Route>
            </Switch>        
        </BrowserRouter>
    )
}

export default Routes