import React from 'react'
import { Link } from 'react-router-dom'
import './Header.css'

function Header() {
    return (
        <header>
            <nav>
                <Link to="/lovometro">Lovometro</Link>
                <Link to="/covid">Covid</Link>
            </nav>
        </header>
    )
}

export default Header
