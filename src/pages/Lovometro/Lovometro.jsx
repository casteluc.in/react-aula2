import React, { useEffect, useState } from 'react'
import { calcula_porcentagem } from '../../services/loveApi'
import Formulario from './components/Formulario/Formulario'
import './Lovometro.css'

import {Link} from 'react-router-dom'
import Header from '../../common/Header/Header'

function Lovometro() {
    const [resposta, setResposta] = useState("")

    useEffect(() => {
        console.log(resposta)
    }, [resposta])

    const handleSubmit = e => {
        e.preventDefault()
        let nome1 = e.target.nome1.value
        let nome2 = e.target.nome2.value
        
        calcula_porcentagem(nome1, nome2, setResposta)
    }

    return(
        <>  
            <Header/>
            <main>
                <Formulario submit={handleSubmit}/>
                <h1>{resposta.fname}</h1>
                <h1>{resposta.sname}</h1>
                <h1>{resposta.percentage}</h1>
                <h1>{resposta.result}</h1>
                <Link to="/covid">testando</Link>
            </main>
        </>
    )
}

export default Lovometro