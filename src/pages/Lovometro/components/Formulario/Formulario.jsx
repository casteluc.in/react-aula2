import React from 'react'

function Formulario({submit}) {
    return(
        <form onSubmit={submit}>
            <input type="text" name="nome1"/>
            <input type="text" name="nome2"/>
            <input type="submit" value="Calcular compatibilidade"/>
        </form>
    )
}

export default Formulario