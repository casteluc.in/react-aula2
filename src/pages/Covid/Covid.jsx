import React, { useEffect, useState } from 'react'
import Header from '../../common/Header/Header'
import { pega_dados_covid } from '../../services/covidApi'
import './Covid.css'

function Covid() {
    const [estado, setEstado] = useState({})

    useEffect(() => {
        pega_dados_covid(setEstado)
    }, [])

    useEffect(() => {
        console.log(estado)
    }, [estado])

    return(
        <>
        <Header/>
        <main>
            <h1>PAGINA DO COVID</h1>
            <h1>MORTES: {estado.data && estado.data.deaths}</h1>
            <h1>INFECTADOS: {estado.data && estado.data.confirmed}</h1>
        </main>
        </>
    )
}

export default Covid